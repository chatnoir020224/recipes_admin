# ActiveAdminを使ったレシピ管理  
* 頻繁に作るメニューのレシピをExcelやスプレッドシートで管理していたのですが、  
ポートフォリオ作成がてら料理のレシピ管理画面を作成しようと作成しました。  

* 設計や実装の詳しい内容は下記記事に掲載しています。  
https://sunday-engineer.hatenadiary.com/archive/category/ActiveAdmin

---

* docker-compose init
    * docker-compose up -d --build
    * docker-compose exec web bash
    * yarn install --check-files
    * bin/rails db:create
    * bin/rails db:migrate
    * bin/rails db:migrate RAILS_ENV=test
    * gem install mailcatcher

* よく使うコマンド
    * bin/rails s -b 0.0.0.0
    * mailcatcher --ip 0.0.0.0

* アクセスURL
    * http://localhost:3000/admin 