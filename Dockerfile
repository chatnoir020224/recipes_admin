FROM ruby:2.6
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update -qq && \
    apt-get install -y build-essential \
    # qt5-default \
    libqt5webkit5-dev \
    gstreamer1.0-plugins-base \
    gstreamer1.0-tools \
    gstreamer1.0-x \
    postgresql-client \
    xvfb \
    imagemagick \
    redis-tools \
    zip \
    unzip \
    vim \
    fonts-migmix fonts-liberation libasound2 libatk-bridge2.0-0 libatspi2.0-0 libgbm1 libgtk-3-0 libnspr4 libnss3 libxkbcommon0 xdg-utils
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# change lang
ENV LANG C.UTF-8

# change timezone
RUN ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# # install chrome
RUN wget -q -O /tmp/google-chrome-stable.deb 'https://www.slimjet.com/chrome/download-chrome.php?file=files%2F90.0.4430.72%2Fgoogle-chrome-stable_current_amd64.deb' && \
    dpkg --add-architecture amd64 &&\
    dpkg -i --force-depends /tmp/google-chrome-stable.deb && \
    rm /tmp/google-chrome-stable.deb

# install NVM & Install JS pakcage
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
RUN . ~/.bashrc && \
    nvm install 10.23.0 && \
    nvm use 10.23.0 && \
    npm install -g yarn

WORKDIR /recipes_admin
COPY Gemfile /recipes_admin/Gemfile
COPY Gemfile.lock /recipes_admin/Gemfile.lock
RUN gem install bundler
RUN bundle install
